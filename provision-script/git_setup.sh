#!/bin/bash
GIT_VETSION=2.13.3
echo "git-install"

# git
if type /usr/local/bin/git 2>/dev/null 1>/dev/null ;then
    /usr/local/bin/git --version
    exit 0
fi
# wget 
if ! type wget 2>/dev/null 1>/dev/null ;then
    yum install -y wget
fi
# install
yum install -y curl-devel expat-devel openssl-devel perl-ExtUtils-MakeMaker
wget https://www.kernel.org/pub/software/scm/git/git-${GIT_VETSION}.tar.gz
tar zxvf git-${GIT_VETSION}.tar.gz
cd git-${GIT_VETSION}
make prefix=/usr/local install
/usr/local/bin/git --version
cd ..
rm -rf git-${GIT_VETSION}
rm git-${GIT_VETSION}.tar.gz
