#!/bin/bash
DOCKER_COMPOSE_VETSION=1.14.0

# docker-engin1

if ! type docker 2>/dev/null 1>/dev/null ;then
    yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
    sudo yum-config-manager --disable docker-ce-edge
    yum -y install docker-ce 
    systemctl start docker
    systemctl enable docker
fi

# docker-compose
if type /usr/local/bin/docker-compose 2>/dev/null 1>/dev/null ;then
    /usr/local/bin/docker-compose --version
    exit 0
fi

curl -L https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VETSION}/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
/usr/local/bin/docker-compose --version
